# Ansible Role: Zoom

This role installs [Zoom](https://zoom.us/) binary on any supported host.

## Requirements

None

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    zoom_package_name: zoom
    zoom_package_url: https://zoom.us/client/latest
    zoom_gpg_url: https://zoom.us/linux/download/pubkey
    zoom_gpg_fingerprint: 3960 60CA DD8A 7522 0BFC B369 B903 BF18 61A7 C71D

This role always installs the latest version. See [available Zoom releases](https://zoom.us/download).

The name of the package.

    zoom_package_name: zoom

The url which the package will be downloaded.

    zoom_package_url: https://zoom.us/client/latest

The url of the GPG Key (rpm based distros only).

    zoom_gpg_url: https://zoom.us/linux/download/pubkey

The fingerprint of the GPG Key (rpm based distros only).

    zoom_gpg_fingerprint: 3960 60CA DD8A 7522 0BFC B369 B903 BF18 61A7 C71D

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: zoom

## License

MIT / BSD
